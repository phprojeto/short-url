<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('/create', function () {
    return view('url/create');
});*/

Route::get('/shorturl', 'ShortUrlController@create')->name('shorturl.create');

Route::post('/shorturl', 'ShortUrlController@store')->name('shorturl.store');

Route::get('/redirect/{shorturl}', 'ShortUrlController@redirect')->name('shorturl.redirect');