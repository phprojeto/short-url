<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Short Url</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <form method="POST" action="{{ route('shorturl.store') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label for="original_url">Url</label>
                 <input type="text" class="form-control" id="original_url" aria-describedby="url_help" placeholder="www.example.com" name="original_url" @if(isset($originalUrl)) value="{{ $originalUrl }}" @endif>
                <small id="url_help" class="form-text text-muted">Copy your url here.</small>
            </div>
            <div class="form-group">
                <label for="short_url">Short Url</label>
                <input type="text" class="form-control" id="short_url" readonly="" @if(isset($shortUrl)) value="{{ $shortUrl }}" @endif>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</body>
</html>