<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Url;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class ShortUrlController extends Controller
{
    public function create()
    {
    	return view ('url.create');
    }

    public function store(Request $request)
    {
    	$hash = hash("md5", rand() . $request->input('original_url'));
    	$shortUrl = new Url;

    	$shortUrl->original_url = $request->original_url;
    	$shortUrl->short_url = substr($hash, -10);
    	$shortUrl->save();
    	//dd($shortUrl);
    	return view('url.create', [
			'originalUrl' => $request->original_url,
			'shortUrl' => substr($hash, -10)
    	]);
    }

    public function redirect($shortUrl)
    {

    	$originalUrl = DB::table('urls')
    	->select('original_url')
    	->where('short_url', $shortUrl)
    	->first();
    	return Redirect::to($originalUrl->original_url);
    	//dd($shortUrl);
    }

}
